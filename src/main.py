import psycopg2
import select
from docker import from_env
from .env import *
from time import sleep
from collections import deque

docker = from_env()


core_container = docker.containers.get(os.environ['HOSTNAME'])

try:
    network = docker.networks.get(AMY)
except:
    network = docker.networks.create(AMY)
    network.connect(core_container)
    docker.containers.run('registry.gitlab.com/amy-assistant/core/db', name=os.environ['AMY_DB_HOST'], network=network.name,
                          detach=True, environment={'AMY': AMY})
    docker.containers.run(
        'mongo', name=os.environ['AMY_MDB_HOST'], network=network.name, detach=True, environment={'AMY': AMY}, command=f'''mongod --replSet {AMY}-set && sleep 2 && mongo --eval "rs.initiate()
            rs.status()"''')

    docker.containers.run('rabbitmq', name=os.environ['AMY_Q_HOST'], network=network.name,
                          detach=True, environment={'AMY': AMY})

    docker.containers.run('registry.gitlab.com/amy-assistant/gate', name=f'{AMY}-gate', network=network.name,
                          detach=True, environment={'AMY': AMY}, ports={80: 8080})

    docker.containers.run('registry.gitlab.com/amy-assistant/api', name=f'{AMY}-api', network=network.name,
                          detach=True, environment={'AMY': AMY})

    docker.containers.run('registry.gitlab.com/amy-assistant/worker', name=f'{AMY}-worker', network=network.name,
                          detach=True, environment={'AMY': AMY})

    docker.containers.run('registry.gitlab.com/amy-assistant/client', name=f'{AMY}-client', network=network.name,
                          detach=True, environment={'AMY': AMY})


sleep(10)

connection = psycopg2.connect(host=AMY_DB_HOST, dbname=AMY_DB,
                              user=AMY, password=AMY)


connection.set_isolation_level(
    psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
connection.notifies = deque(connection.notifies)

with connection.cursor() as cursor:
    cursor.execute(f'LISTEN plugin;')


def onChange(notify):
    print(notify.payload)


while True:
    if not select.select([connection], [], [], 5) == ([], [], []):
        connection.poll()
        while connection.notifies:
            onChange(connection.notifies.pop())
