import os

PRODUCTION = os.environ.get('PRODUCTION', False)

AMY = os.environ.get('AMY', 'amy')
AMY_DB = os.environ.get('AMY_DB', AMY)
AMY_DB_HOST = os.environ.get(
    'AMY_DB_HOST', AMY + '-db') if PRODUCTION else 'localhost'
AMY_DB_USER = os.environ.get('AMY_DB_USER', AMY)
AMY_DB_PASSWORD = os.environ.get('AMY_DB_PASSWORD', AMY)
